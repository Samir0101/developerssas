require("./bootstrap");

import vue from "vue";

window.Vue = vue;

import App from "./components/App.vue";
// importamos Axios
import VueAxios from "vue-axios";
import axios from "axios";
// importamosyconfiguramos el vue-router
import router from "./router";

import { createApp } from 'vue'

createApp(App).use(VueAxios, axios).use(router).mount('#app')
