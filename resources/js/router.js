import { createRouter, createWebHistory } from 'vue-router'

const Main = () => import("./components/employee/Main.vue");
const Create = () => import("./components/employee/Create.vue");
const Details = () => import("./components/employee/Details.vue");

export const routes = [
    {
        name: "main",
        path: "/main",
        component: Main,
    },
    {
        name: "create",
        path: "/create",
        component: Create,
    },
    {
        name: "details",
        path: "/details/:id",
        component: Details,
    },
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

export default router;
