<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'address',
        'position',
        'salary',
        'skills'
    ];

    public function scopeSalary($query, $from, $to)
    {
        if (!is_null($from) && !is_null($to))
            return $query->whereBetween('salary', [$from, $to]);
    }

    public function scopeEmail($query, $value)
    {
        if (!is_null($value))
            return $query->where('email',  'like', '%'.$value.'%');
    }


}
